class UserMailer < ApplicationMailer
  def welcome(user)
    @user = user
    mail(to: @user.email, subject: 'Welcome to ClockWork')
  end

  def verification_instructions(user)
    @user = user
    @verification_url = verify_url(@user.perishable_token)
    mail(to: @user.email, subject: 'Verify your account at ClockWork')
  end
end
