class UsersController < ApplicationController
  before_action :authenticate_actions!, only: [:show, :update]

  def show
    present User::Update
  end

  def new
    form User::Create
  end

  def create
    run User::Create do |_operation|
      return redirect_to root_path
    end

    @form.prepopulate!
    render :new
  end

  def update
    run User::Update do |operation|
      return respond_to do |format|
        format.json { respond_with_bip(@form) }
      end
    end

    respond_to do |format|
      format.json { respond_with_bip(@form) }
    end
  end

  def verify
    run User::Verify do |_op|
      flash[:notice] = 'Verification successful!'
      return redirect_to root_path
    end

    flash[:error] = 'User not found!'
    redirect_to root_path
  end
end
