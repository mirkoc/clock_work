class ApplicationController < ActionController::Base
  after_action :prepare_unobtrusive_flash
  before_action :add_current_user_to_params
  helper_method :tyrant
  protect_from_forgery with: :exception

  def tyrant
    Tyrant::Session.new(request.env['warden'])
  end

  private

  def add_current_user_to_params
    params.merge!(current_user: tyrant.current_user)
  end

  def authenticate_actions!
    return true if tyrant.signed_in?
    flash[:warning] = 'You have to sign in first!'
    redirect_to login_path
  end
end
