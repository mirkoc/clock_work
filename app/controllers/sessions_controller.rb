class SessionsController < ApplicationController
  before_action :already_signed_in?, only: [:new, :create]

  def new
    form Session::SignIn
  end

  def create
    run Session::SignIn do |operation|
      tyrant.sign_in!(operation.model)
      flash[:notice] = 'Successful sign in!'
      return redirect_to root_path
    end

    render action: :new
  end

  def destroy
    run Session::SignOut do
      tyrant.sign_out!
      flash[:notice] = 'Successful sign out!'
      redirect_to root_path
    end
  end

  private

  def already_signed_in?
    return true unless tyrant.signed_in?
    flash[:warning] = 'Already signed in!'
    redirect_to root_path
  end
end
