class OrganizationsController < ApplicationController
  def show
  end

  def new
    form Organization::Create
  end

  def create
    run Organization::Create do |_operation|
      flash[:notice] = 'Organization successfully created!'
      return redirect_to root_url
    end

    @form.prepopulate!
    render :new
  end
end
