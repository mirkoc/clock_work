module Session
  class SignIn < Trailblazer::Operation
    contract do
      undef :persisted?
      property :email, virtual: true
      property :password, virtual: true
      validates :email, :password, presence: true
      validate :password_ok?

      attr_reader :user

      private

      def password_ok?
        return if email.blank? || password.blank?
        @user = User.find_by_email(email)
        return if @user && Tyrant::Authenticatable.new(@user).digest?(password)
        errors.add(:password, 'Wrong password!')
      end
    end

    def process(params)
      validate(params[:session]) do |contract|
        @model = contract.user
      end
    end
  end

  class SignOut < Trailblazer::Operation
    def process(_params)
    end
  end
end
