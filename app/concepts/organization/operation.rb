class Organization < ApplicationRecord
  class Create < Trailblazer::Operation
    include Resolver
    include Model
    model Organization, :create
    policy Organization::Policy, :create?

    contract do
      property :name
      property :website_url
      validates :name, presence: true
    end

    def process(params)
      validate(params[:organization]) do |f|
        f.model.code = SecureRandom.uuid
        f.model.creator = policy.user
        f.save
      end
    end
  end
end
