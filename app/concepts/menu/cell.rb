class Menu::Cell < Cell::Concept
  def show
    render :show
  end

  private

  def share_link
    link_to '', "http://www.facebook.com/sharer.php?u=#{::Rails.application.secrets.host}",
            class: 'menu-item menu-item-color fa fa-share-alt', target: :blank
  end

  def about_link
    link_to '', about_path, class: 'menu-item menu-item-color fa fa-info-circle'
  end

  def action_link
    if model
      link_to '', user_path(model),
              class: 'menu-item menu-item-color fa fa-paperclip'
    else
      link_to '', register_path,
            class: 'menu-item menu-item-color fa fa-user-plus'
    end
  end

  def github_link
    link_to '', 'https://github.com/floatingpointio',
            class: 'menu-item menu-item-color fa fa-github', target: :blank
  end

  def home_link
    link_to '', root_path, class: 'menu-item menu-item-color fa fa-home'
  end
end
