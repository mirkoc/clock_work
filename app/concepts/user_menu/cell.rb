class UserMenu::Cell < Cell::Concept
  def show
    if model
      render :show
    else
      render :login_link
    end
  end

  private

  def display_name
    model.full_name
  end

  def display_email
    model.email
  end

  def my_data_link
    link_to ' My data ', user_path(model),
            class: 'fa fa-paperclip dropdown-item text-uppercase'
  end

  def my_organizations_link
    link_to ' My organizations ', '',
            class: 'fa fa-bank dropdown-item text-uppercase'
  end

  def logout_link
    link_to ' Log out ', logout_path, 'data-method' => :delete,
            class: 'fa fa-sign-out dropdown-item text-uppercase text-muted'
  end

  def avatar_url
    default_url = 'https://clock-work-dev.herokuapp.com/images/avatar.jpg'
    gravatar_id = model.email ? Digest::MD5.hexdigest(model.email.downcase) : nil
    "http://gravatar.com/avatar/#{gravatar_id}"\
      ".png?s=48&d=#{CGI.escape(default_url)}"
  end

  def login_link
    link_to 'Login', login_path, class: 'nav-link'
  end
end
