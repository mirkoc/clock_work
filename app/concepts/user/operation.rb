class User < ApplicationRecord
  require 'reform/form/validation/unique_validator'
  class Create < Trailblazer::Operation
    include Model
    model User, :create

    contract do
      property :email
      property :password, virtual: true
      property :password_confirmation, virtual: true
      property :full_name

      validates :email, :password, :password_confirmation, presence: true
      validates :email, unique: true
      validate :password_ok?

      def password_ok?
        return unless email && password
        return if password == password_confirmation
        errors.add(:password, 'Passwords don\'t match')
      end
    end

    def process(params)
      validate(params[:user]) do |contract|
        create!
        contract.save
      end
    end

    def create!
      auth = Tyrant::Authenticatable.new(contract.model)
      auth.digest!(contract.password)
      auth.confirmed!
      auth.sync
    end
  end

  class Update < Trailblazer::Operation
    include Model
    model User, :update

    contract do
      property :email
      property :full_name
      validates :email, presence: true, if: -> { email }
      validate :email_unique?

      def email_unique?
        return unless email && @model
        user_not_exists = User.where(email: email).where.not(id: @model.id).first.nil?
        return if user_not_exists
        errors.add(:email, 'Already in use!')
      end
    end

    def process(params)
      validate(params[:user]) do |f|
        f.save
      end
    end
  end
end
