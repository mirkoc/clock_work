class Organization < ApplicationRecord
  belongs_to :creator, foreign_key: :user_id, class_name: User, optional: true
  has_many :users
end
