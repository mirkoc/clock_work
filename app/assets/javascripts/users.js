//= require jquery-ui

(function($) {
  $(document).ready(function() {
      /* Activating Best In Place */
      jQuery(".best_in_place").best_in_place();
  });
  mirko = null;
  $(document).bind("ajax:success", function (evt, data, status, xhr) {
    $("#"+evt.target.id).effect( "highlight", {color:"#c9eb86"}, 3000 );
  });

  $(document).bind("ajax:error", function (evt, data, status, xhr) {
    console.log(data)
    console.log($.parseJSON(data.responseText))
    $.each($.parseJSON(data.responseText), function(index, value){
      UnobtrusiveFlash.showFlashMessage(value, {type: 'error'});
    });
    $("#"+evt.target.id).effect( "highlight", {color:"#c9eb86"}, 3000 );
  });
})(jQuery);