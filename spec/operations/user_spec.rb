require 'rails_helper'

RSpec.describe User::Create, type: :operation do
  describe 'Create' do
    it 'creates a new user record' do
      user = User::Create.(user: {username: 'tester',
                                  email: 'tester@example.com',
                                  full_name: 'Tom the Tester',
                                  password: '12345678',
                                  password_confirmation: '12345678'}).model
      expect(user.id).to be
      expect(User.find_by_id(user.id)).to eq(user)
      expect(user.persisted?).to eq(true)
    end
  end
end
user = User::Create.(user: {username: 'tester', email: 'tester@example.com', full_name: 'Tom the Tester', password: '12345678', password_confirmation: '12345678'})