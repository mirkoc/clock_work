require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ClockWork
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    # postmarkapp
    config.action_mailer.delivery_method = :postmark
    config.action_mailer.postmark_settings = { api_token: 'b30d782a-3ad0-4d46-857a-09f805fad7be' }
  end
end
