# File here config/initializers/simple_form_bootstrap.rb
SimpleForm.setup do |config|
  config.label_text = lambda { |label, required, explicit_label| "#{label}" }
  config.required_by_default = false
  config.wrappers :vertical_input_group, tag: 'fieldset', class: 'form-group', error_class: 'has-danger' do |b|
    b.use :html5
    b.use :placeholder
    b.use :input, class: 'form-control', error_class: 'form-control-danger'
    b.use :error, wrap_with: { tag: 'span', class: 'text-help' }
    b.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }

  end

  config.wrappers :horizontal_input_group, tag: 'div', class: 'form-group', error_class: 'has-error' do |b|
    b.use :html5
    b.use :placeholder
    b.use :label, class: 'col-sm-3 control-label'

    b.wrapper tag: 'div', class: 'col-sm-9' do |ba|
      ba.wrapper tag: 'div', class: 'input-group col-sm-12' do |append|
        append.use :input, class: 'form-control'
      end
      ba.use :error, wrap_with: { tag: 'span', class: 'help-block' }
      ba.use :hint,  wrap_with: { tag: 'p', class: 'help-block' }
    end
  end
end