Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
  get 'about' => 'home#about'
  # user endpoints
  resources :users, only: [:show, :update]
  get 'register' => 'users#new'
  post 'register' => 'users#create'
  get 'verify' => 'users#verify'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  # organization endpoints
  resources :organizations, only: [:show, :index]
  get 'create/organization' => 'organizations#new'
  post 'create/organization' => 'organizations#create'
end
