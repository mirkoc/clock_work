class CreateOrganizations < ActiveRecord::Migration[5.0]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :code
      t.string :website_url
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
